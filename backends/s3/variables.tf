
variable "region" {
  type        = string
  description = "The AWS region all resources are deployed to"
  default     = "eu-central-1"
}

variable "environment" {
  type        = string
  description = "Target environment to deploy all resources to"
  default     = "staging"
}

variable "tags" {
  type        = map
  description = "The default tags to be assigned to all resources created by this template"
  default     = {}
}
