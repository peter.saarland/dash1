provider "aws" {
  region = var.region
}

module "terraform_state_backend" {
  source    = "git::https://github.com/cloudposse/terraform-aws-tfstate-backend.git?ref=0.16.0"
  namespace = "dash1"
  stage     = var.environment
  name      = "state-backend"
  region    = var.region
  tags      = var.tags
}
