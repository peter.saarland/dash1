terraform {
  backend "s3" {
    region         = "eu-central-1"
    bucket         = "dash1-staging-state-backend-state"
    key            = "terraform.tfstate"
    dynamodb_table = "dash1-staging-state-backend-state-lock"
    encrypt        = true
  }
}

provider "aws" {
  region = "eu-central-1"
}

module "dash1-aws" {
  source              = "git::https://gitlab.com/peter.saarland/dash1.git//modules/aws"
  region              = "eu-central-1"
  availability_zone   = "eu-central-1b"
  instance_image_ami  = "ami-0e342d72b12109f91"
  environment         = "test"
  instance_type       = "t2.large"
  manager_count       = 2
  worker_count        = 2
  satellite_count     = 2
  ssh_public_key_file = var.ssh_public_key_file
  tags = {
    purpose = "Example dash1 implementation"
  }
}
