# Example to use dash1 on AWS with an S3 state backend

## How to run the example

- cd into [`backend/s3`](../../backend/s3)
- export TF_VAR_* for all required variables
- `terraform init`
- `terraform apply -auto-approve`
- Copy the dynamodb table and S3 bucket name from the outputs
- Paste them into the `dynamodb_table` and `bucket` property, which are part of the `backend "s3"` block in `main.tf`
- cd into this folder
- run `terraform init`
- run `terraform apply -auto-approve`

## Troubleshooting

When facing a `NoSuchBucket` error (404), delete all local state and the `.terraform` directory from this folder.