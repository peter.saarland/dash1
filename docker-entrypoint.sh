#! /bin/bash
set -e

if [ -d $ENVIRONMENT_DIR ];
then
  # ToDo: replace this with `if0 environment load`
  env_files=`find $ENVIRONMENT_DIR -type f -name "*.env"`

  for file in $env_files;
  do
    set -o allexport
    #source $file
    export $(grep -hv '^#' $file | xargs)
    set +o allexport
  done
fi

DASH1_SSH_DIR=${DASH1_SSH_DIR:-/.ssh}
if [ -d "$DASH1_SSH_DIR" ]; then
    if [ "$(ls -A $DASH1_SSH_DIR)" ]; then
        echo "Found SSH Keys in $DASH1_SSH_DIR"
        cp /.ssh/* /root/.ssh/. && chmod 0600 /root/.ssh/id_rsa
        echo "Copied SSH Keys to /root/.ssh"
    fi
fi

exec "$@"