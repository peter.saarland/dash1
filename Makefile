SHELL := bash
.ONESHELL:
#.SILENT:
.SHELLFLAGS := -eu -o pipefail -c
#.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
.DEFAULT_GOAL := help

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

# Defaults
SSH_PUBLIC_KEY_FILE ?= "${HOME}/.ssh/id_rsa.pub"
SSH_PRIVATE_KEY_FILE ?= "${HOME}/.ssh/id_rsa"
IF0_ENVIRONMENT ?= zero
DOCKER_SHELLFLAGS ?= run --rm -it -e IF0_ENVIRONMENT=${IF0_ENVIRONMENT} --name dash1-zero -v ${PWD}:/dash1 -v ${HOME}/.if0/.environments/${IF0_ENVIRONMENT}:/root/.if0/.environments/zero -v ${HOME}/.gitconfig:/root/.gitconfig dash1
DASH1_MODULE ?= generic
export DOCKER_BUILDKIT=1
ENVIRONMENT_DIR ?= ${HOME}/.if0/.environments/zero
PROVIDER_UPPERCASE=$(shell echo $(DASH1_MODULE) | tr  '[:lower:]' '[:upper:]')
TF_STATE_PATH=${ENVIRONMENT_DIR}/dash1.tfstate
TF_PLAN_PATH=${ENVIRONMENT_DIR}/dash1.plan
export TF_LOG ?= 
export TF_IN_AUTOMATION=1
export TF_VAR_environment=${IF0_ENVIRONMENT}
VERBOSITY ?= 0

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: load
load: /tmp/.loaded.sentinel

/tmp/.loaded.sentinel: $(shell find ${ENVIRONMENT_DIR} -type f -name '*.env') ## help
> @if [ ! -z $$IF0_ENVIRONMENT ]; then echo "Loading Environment ${IF0_ENVIRONMENT}"; fi
> @if [ ! -z $$DASH1_MODULE ]; then echo "Loading Provider ${DASH1_MODULE}"; else echo "No Provider selected. Exit."; exit 1; fi
> @touch /tmp/.loaded.sentinel

modules/${DASH1_MODULE}/.terraform: /tmp/.loaded.sentinel $(shell find modules/${DASH1_MODULE}/*.tf -type f)
> @cd modules/${DASH1_MODULE}
>	@terraform init -compact-warnings -input=false

/tmp/.validated.sentinel: modules/${DASH1_MODULE}/.terraform
>	@cd modules/${DASH1_MODULE}
> @export TF_VAR_environment=${IF0_ENVIRONMENT}
>	@terraform validate > /dev/null
> @touch /tmp/.validated.sentinel

.PHONY: plan
plan: ${TF_PLAN_PATH} ## Plan

# Plan
${TF_PLAN_PATH}: /tmp/.validated.sentinel
> @cd modules/${DASH1_MODULE}
> @terraform plan -lock=true -compact-warnings -input=false -out=${TF_PLAN_PATH} -state=${TF_STATE_PATH} 

.PHONY: apply
apply: plan ${TF_STATE_PATH}

${TF_STATE_PATH}: ${TF_PLAN_PATH}
> @cd modules/${DASH1_MODULE}
> terraform apply -compact-warnings -state=${TF_STATE_PATH} -auto-approve ${TF_PLAN_PATH}

.PHONY: destroy
destroy: 
> @cd modules/${DASH1_MODULE}
> @terraform destroy -compact-warnings -state=${TF_STATE_PATH} -auto-approve 
> @rm -rf /tmp/.*.sentinel
> @rm -rf ${TF_STATE_PATH} ${TF_STATE_PATH}.backup ${TF_PLAN_PATH} ${ENVIRONMENT_DIR}/dash1-zero.env

# Zero
.PHONY: infrastructure
infrastructure: ${ENVIRONMENT_DIR}/dash1-zero.env ## Generate the configuration for zero

${ENVIRONMENT_DIR}/dash1-zero.env: ${TF_STATE_PATH}
> @cd modules/${DASH1_MODULE}
> @terraform output -state=${TF_STATE_PATH} | tr -d ' ' > ${ENVIRONMENT_DIR}/dash1-zero.env

.PHONY: output
output:
> @cd modules/${DASH1_MODULE}
> terraform output -state=${TF_STATE_PATH} | tr -d ' '

.PHONY: show
show:
> @cd modules/${DASH1_MODULE}
> @terraform show ${TF_STATE_PATH}

# Development
.PHONY: build
build:
> @docker build --pull -t dash1 .

.PHONY: dev
dev: .SHELLFLAGS = ${DOCKER_SHELLFLAGS}
dev: SHELL := docker
dev:
> @bash

.PHONY: ssh
ssh: ${ENVIRONMENT_DIR}/.ssh/id_rsa ${ENVIRONMENT_DIR}/.ssh/id_rsa.pub

${ENVIRONMENT_DIR}/.ssh/:
> @mkdir ${ENVIRONMENT_DIR}/.ssh/

${ENVIRONMENT_DIR}/.ssh/id_rsa ${ENVIRONMENT_DIR}/.ssh/id_rsa.pub: ${ENVIRONMENT_DIR}/.ssh/
> @ssh-keygen -b 4096 -t rsa -q -N "" -f ${ENVIRONMENT_DIR}/.ssh/id_rsa 