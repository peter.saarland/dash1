# dash1

This project provides the definition of (virtual) infrastructure to run [**Zero**](https://gitlab.com/peter.saarland/zero) as *infrastructure as code* (IaC). The primary tool for this is [Terraform](https://www.terraform.io/).

## Get Started

For demo purposes, here's how to create 1 small-sized Ubuntu-VM at HETZNER:

```
docker run \
  -e DASH1_PROVIDER=hcloud \
  -e HCLOUD_AUTH_TOKEN=MySecretToken \
  -e HCLOUD_SSH_KEY=d7:72:02:3d:54:3a:ef:c4:8a:b6:e8:36:a5:7b:64:f8
  registry.gitlab.com/peter.saarland/dash1 make apply
```

This will create a VM but won't save the **tfstate** to a persistent file. The recommended way of using `dash1` is with [if0](https://gitlab.com/peter.saarland/if0). Documentation on this is WIP.

## Configuration Options
All configuration is provided in form of **Environment Variables**. `dash1` simply uses existing Environment Variables as its **running-config**. Additionally, configuration can be provided in `dash1.env` and mounted to `/environment` in the container. Variables in `dash1.env` will be loaded **last** and thus have precedence over already existing variables. This is important to remember in CI.

The Minimum Viable Configuration to deploy infrastructure with **dash1** is:

- *_AUTH_TOKEN
- *_SSH_KEY (it's the identifier of the key at the infrastructure provider of choice; e.g. for HETZNER it's the Key's fingerprint)
- DASH1_PROVIDER (e.g. "hcloud", so dash1 knows which provider to use)

`_SSH_KEY` and `_AUTH_TOKEN` are provider-specific. The values needed can be obtained from the [modules](../modules) README files.

```bash
# Terraform Providers
DIGITALOCEAN_AUTH_TOKEN=

HCLOUD_AUTH_TOKEN=

AWS_AUTH_TOKEN=

IBM_AUTH_TOKEN=

# Dash1


# SSH
HCLOUD_SSH_KEY=
AWS_SSH_KEY=
...
```

Additional Configuration Options can be derived from the modules' `README.md` as every module features its own options.