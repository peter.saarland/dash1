## Local testing

⚠️ If you don't have a private SSH Key, run `make ssh-gen` to generate a local SSH key pair

Use the [Makefile](./Makefile) targets to setup/teardown the infrastructure.

⚠️ The `Makefile` does not provide targets for the example and backends.
