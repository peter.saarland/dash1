# dash1 API Specification

- Scope: either `global` or `provider`. Configuration with `Scope: provider` is provider-specific, i.e. defaults and documentation can be obtained from the [modules](../modules) directory. `Scope: global` means that this configuration option is provider-independent

## Input

### Required Configuration



```
# The "Region" where resources will be created in
# Scope: provider
TF_VAR_region=fsn1

# The OS Family of the resource
# Scope: global
# Default: ubuntu
# Possible Values: ubuntu, debian, windows
TF_VAR_os_family=ubuntu

# The Environment of the resource
# when managed with if0
# See https://gitlab.com/peter.saarland/if0
# Scope: global
# Default: zero
# Possible Values: ANY
TF_VAR_environment=zero

# The number of MANAGER instances to be created
# Scope: global
# Default: 1
TF_VAR_manager_instances=1

# The number of WORKER instances to be created
# Scope: global
# Default: 0
TF_VAR_worker_instances=0

# The number of WINDOWS instances to be created
# Note: WINDOWS instances are WORKER instances by default
# Scope: global
# Default: 0
TF_VAR_windows_instances=0

# The SIZE of the instances to be created
# Scope: provider
TF_VAR_manager_size=cx11
TF_VAR_worker_size=cx11

# The number of VOLUMES per instance to be created
# Scope: global
# Default: 0
TF_VAR_volume_count=0

# The size of the VOLUMES to be created
# Scope: global
TF_VAR_volume_size=50

# The LABELS of the resource
# Scope: provider
TF_VAR_labels=zero

# The CLUSTER_NETWORK of the instances
# Scope: provider
TF_VAR_cluster_network_name=zero
TF_VAR_cluster_network_zone=eu-central
TF_VAR_cluster_network=10.16.0.0/16

# The SSH Key provisioned to the instances
# Scope: provider
TF_VAR_ssh_key=
```

## Output

`dash1` modules **MUST** output certain status-values after successful invokation. The output needs to be **uppercase** to be in line with **Environment Variables** syntax/semantics.

All of the following Environment Variables need to be in `output.tf`, even if they are empty. These Environment Variables will be consumed by [Zero](https://gitlab.com/peter.saarland/zero).

```
# Scope: global
ZERO_NODES_MANAGER=127.0.0.1,127.0.0.2
ZERO_NODES_WORKER=127.0.0.3,127.0.0.4
ZERO_NODES_WINDOWS=127.0.0.4

# Scope: provider
ZERO_PROVIDER=hcloud

# Scope: provider
ZERO_CLUSTER_NETWORK=10.16.0.0/16

# Scope: provider
ZERO_INGRESS_IP=127.0.0.1
```